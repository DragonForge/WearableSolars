package tk.zeitheron.wearsfr;

import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import tk.zeitheron.solarflux.init.ItemsSF;
import tk.zeitheron.solarflux.init.SolarsSF;

@RegisterRecipes(modid = InfoWS.MOD_ID)
public class RecipesWS
		extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		Object item = ItemsSF.PHOTOVOLTAIC_CELL_2;
		if(!OreDictionary.getOres("gearIron").isEmpty())
			item = "gearIron";
		shaped(WearableSolars.SOLAR_INJECTOR, "gsg", "chc", "gcg", 'g', item, 's', new ItemStack(SolarsSF.CORE_PANELS[0].getBlock()), 'c', Blocks.IRON_BLOCK, 'h', Items.IRON_HELMET);
		shaped(WearableSolars.SOLAR_EJECTOR, "gsg", "chc", "gcg", 'g', item, 's', Items.IRON_PICKAXE, 'c', Blocks.IRON_BLOCK, 'h', Items.IRON_HELMET);
	}

	@Override
	public void smelting()
	{
	}
}