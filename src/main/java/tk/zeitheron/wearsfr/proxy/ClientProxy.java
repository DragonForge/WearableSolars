package tk.zeitheron.wearsfr.proxy;

import com.zeitheron.hammercore.bookAPI.fancy.ManualCategories;
import com.zeitheron.hammercore.bookAPI.fancy.ManualEntry;
import com.zeitheron.hammercore.bookAPI.fancy.ManualEntry.EnumEntryShape;
import com.zeitheron.hammercore.bookAPI.fancy.ManualEntry.eEntryShape;
import com.zeitheron.hammercore.bookAPI.fancy.ManualPage;
import com.zeitheron.hammercore.bookAPI.fancy.ManualPage.PageType;
import com.zeitheron.hammercore.client.render.item.IItemRender;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;
import tk.zeitheron.solarflux.InfoSF;
import tk.zeitheron.solarflux.block.BlockBaseSolar;
import tk.zeitheron.wearsfr.InfoWS;
import tk.zeitheron.wearsfr.SolarPaneInfo;
import tk.zeitheron.wearsfr.WearableSolars;

import java.util.stream.Collectors;

public class ClientProxy
		extends CommonProxy
		implements IItemRender
{
	@Override
	public void init()
	{
		ItemRenderingHandler.INSTANCE.applyItemRender(this, it -> it instanceof ItemArmor && ((ItemArmor) it).armorType == EntityEquipmentSlot.HEAD);

		ManualCategories.registerCategory(InfoWS.MOD_ID, new ResourceLocation(InfoWS.MOD_ID, "textures/mbg.png"), new ResourceLocation("hammercore", "textures/gui/manual_back.png"));
		new ManualEntry(InfoWS.MOD_ID, InfoWS.MOD_ID, 0, 0, WearableSolars.INFOS.values().stream().map(p ->
		{
			ItemStack stack = new ItemStack(Items.DIAMOND_HELMET);
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString(SolarPaneInfo.SPTAG, p.solar.getBlock().getRegistryName().toString());
			stack.setTagCompound(nbt);
			return stack;
		}).collect(Collectors.toList())).setPages(new ManualPage("hc.manual_desc." + InfoWS.MOD_ID)).setShape(EnumEntryShape.ROUND).registerEntry();
		new ManualEntry(InfoWS.MOD_ID + ":solar_injector", InfoWS.MOD_ID, 0, 2, new ItemStack(WearableSolars.SOLAR_INJECTOR)).setPages(new ManualPage("hc.manual_desc." + InfoWS.MOD_ID + ":solar_injector"), new ManualPage(PageType.NORMAL_CRAFTING, new ItemStack(WearableSolars.SOLAR_INJECTOR))).setShape(eEntryShape.HEX).setParents(InfoWS.MOD_ID).registerEntry();
		new ManualEntry(InfoWS.MOD_ID + ":solar_ejector", InfoWS.MOD_ID, 2, 2, new ItemStack(WearableSolars.SOLAR_EJECTOR)).setPages(new ManualPage("hc.manual_desc." + InfoWS.MOD_ID + ":solar_ejector"), new ManualPage(PageType.NORMAL_CRAFTING, new ItemStack(WearableSolars.SOLAR_EJECTOR))).setShape(eEntryShape.HEX).setParents(InfoWS.MOD_ID + ":solar_injector").registerEntry();
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void tooltip(ItemTooltipEvent e)
	{
		ItemStack stack = e.getItemStack();
		EntityPlayer player = e.getEntityPlayer();

		ItemStack onHead = player != null ? player.inventory.armorItemInSlot(3) : ItemStack.EMPTY;

		SolarPaneInfo spi = SolarPaneInfo.fromHelmet(stack);
		if(spi != null)
		{
			try
			{
				e.getToolTip().add(I18n.format(InfoWS.info("panel"), spi.solar.getBlock().getLocalizedName()));
			} catch(Throwable err)
			{
			}
			e.getToolTip().add(I18n.format(InfoWS.info("energy.generation.max"), spi.getMaxGen()));
		}

		// Perform only for head item
		if(player != null && onHead == stack)
		{
			int gen = SolarPaneInfo.getGeneration(stack, player);
			e.getToolTip().add(I18n.format("info." + InfoSF.MOD_ID + ".energy.generation", gen));
		}
	}

	@Override
	public void renderItem(ItemStack item)
	{
	}

	@Override
	public void renderItem(ItemStack stack, IBakedModel bakedmodel, TransformType transform)
	{
		if(transform == TransformType.GUI)
		{
			SolarPaneInfo spi = SolarPaneInfo.fromHelmet(stack);

			if(spi != null)
			{
				TextureAtlasSprite s = t_top(spi.solar.getBlock());
				UtilsFX.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
				GL11.glPushMatrix();
				GL11.glTranslatef(2, 2, -300);
				GL11.glScalef(.75F, .75F, 1F);
				GlStateManager.disableLighting();
				RenderUtil.drawTexturedModalRect(0, 0, s, 16, 16);
				GlStateManager.enableLighting();
				GL11.glPopMatrix();
			}
		}
	}

	public TextureAtlasSprite t_top(BlockBaseSolar block)
	{
		return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(block.getRegistryName().getNamespace() + ":blocks/" + block.getRegistryName().getPath() + "_top");
	}
}