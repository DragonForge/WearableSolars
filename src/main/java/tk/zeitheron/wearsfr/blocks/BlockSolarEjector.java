package tk.zeitheron.wearsfr.blocks;

import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.utils.WorldUtil;
import tk.zeitheron.wearsfr.SolarPaneInfo;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSolarEjector extends BlockDeviceHC<TileSolarEjector> implements IBlockHorizontal
{
	public BlockSolarEjector()
	{
		super(Material.IRON, TileSolarEjector.class, "solar_ejector");
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileSolarEjector t = WorldUtil.cast(worldIn.getTileEntity(pos), TileSolarEjector.class);
		if(t != null)
		{
			ItemStack held = playerIn.getHeldItem(hand);
			ItemStack ins = t.inv.getStackInSlot(0);
			if(!held.isEmpty() && ins.isEmpty() && SolarPaneInfo.fromHelmet(held) != null)
			{
				t.inv.setInventorySlotContents(0, held.copy());
				t.inv.getStackInSlot(0).setCount(1);
				held.shrink(1);
			}
		}
		return true;
	}
}