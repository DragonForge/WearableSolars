package tk.zeitheron.wearsfr.blocks;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.IRenderableInfo;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;
import tk.zeitheron.wearsfr.InfoWS;
import tk.zeitheron.wearsfr.SolarPaneInfo;
import tk.zeitheron.wearsfr.util.TipUts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TileSolarInjector
		extends TileSyncableTickable
		implements IEnergyStorage, ITileDroppable, ITooltipProviderHC
{
	public final InventoryDummy inv = new InventoryDummy(2);
	public final FEEnergyStorage fe = new FEEnergyStorage(4000);

	public int prevProgress = 0;
	public int progress = 0;

	public int getCraftTime()
	{
		return 20;
	}

	@Override
	public void tick()
	{
		prevProgress = progress;

		if(ticksExisted % 8 == 0)
			setTooltipDirty(true);

		if(canInject())
		{
			if(fe.getEnergyStored() >= 200 && atTickRate(20) && fe.extractEnergy(200, false) >= 200)
			{
				progress++;
				setTooltipDirty(true);
				sendChangesToNearby();
			}
			if(!world.isRemote && progress >= getCraftTime())
			{
				progress -= getCraftTime();
				inject();
				sendChangesToNearby();
			}
		} else if(progress > 0)
			--progress;
	}

	public boolean canInject()
	{
		ItemStack helm = inv.getStackInSlot(0);
		ItemStack panel = inv.getStackInSlot(1);
		return !helm.isEmpty() && helm.getItem() instanceof ItemArmor && ((ItemArmor) helm.getItem()).armorType == EntityEquipmentSlot.HEAD && !panel.isEmpty() && SolarPaneInfo.from(panel.getItem()) != null;
	}

	public void inject()
	{
		ItemStack helm = inv.getStackInSlot(0);
		ItemStack panel = inv.getStackInSlot(1);

		ItemStack nst = helm.copy();
		nst.setCount(1);
		NBTTagCompound tag = nst.hasTagCompound() ? nst.getTagCompound() : new NBTTagCompound();
		tag.setString(SolarPaneInfo.SPTAG, panel.getItem().getRegistryName().toString());
		nst.setTagCompound(tag);
		helm.shrink(1);
		panel.shrink(1);

		EnumFacing face = WorldUtil.getFacing(getLocation().getState());
		Vec3d nv = new Vec3d(pos).add(.5 + face.getXOffset() * .65, .5, .5 + face.getZOffset() * .65);

		if(!world.isRemote)
		{
			EntityItem ei = new EntityItem(world, nv.x, nv.y, nv.z, nst);
			ei.motionY = 0;
			ei.motionX = face.getXOffset() * .1;
			ei.motionZ = face.getZOffset() * .1;
			world.spawnEntity(ei);
		}
	}

	private void randomizeTraj(EntityItem ei)
	{

	}

	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		inv.drop(world, pos);
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		prevProgress = nbt.getInteger("PrevProgress");
		inv.readFromNBT(nbt.getCompoundTag("Items"));
		progress = nbt.getInteger("Progress");
		fe.readFromNBT(nbt);
		setTooltipDirty(true);
	}

	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", inv.writeToNBT(new NBTTagCompound()));
		nbt.setInteger("PrevProgress", prevProgress);
		nbt.setInteger("Progress", progress);
		fe.writeToNBT(nbt);
	}

	@Override
	protected IItemHandler createSidedHandler(EnumFacing side)
	{
		return itemHandlers[side.ordinal()] = new InvWrapper(inv);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}

	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return fe.receiveEnergy(maxReceive, simulate);
	}

	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}

	@Override
	public int getEnergyStored()
	{
		return fe.getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored()
	{
		return fe.getMaxEnergyStored();
	}

	@Override
	public boolean canExtract()
	{
		return false;
	}

	@Override
	public boolean canReceive()
	{
		return true;
	}

	public boolean dirty = false;

	@Override
	public boolean isTooltipDirty()
	{
		return dirty;
	}

	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.dirty = dirty;
	}

	@Override
	public void addInformation(ITooltip tip)
	{
		List<IRenderableInfo> sum = new ArrayList<>();
		if(!inv.getStackInSlot(0).isEmpty())
			sum.add(new ItemStackTooltipInfo(inv.getStackInSlot(0), 16, 16));
		if(!inv.getStackInSlot(1).isEmpty())
			sum.add(new ItemStackTooltipInfo(inv.getStackInSlot(1), 16, 16));
		TipUts.join(tip, '+', sum);

		if(canInject())
		{
			ItemStack helm = inv.getStackInSlot(0);
			ItemStack panel = inv.getStackInSlot(1);

			ItemStack nst = helm.copy();
			nst.setCount(1);
			NBTTagCompound tag = nst.hasTagCompound() ? nst.getTagCompound() : new NBTTagCompound();
			tag.setString(SolarPaneInfo.SPTAG, panel.getItem().getRegistryName().toString());
			nst.setTagCompound(tag);

			tip.append(new StringTooltipInfo(" = "));
			tip.append(new ItemStackTooltipInfo(nst, 16, 16));
		}

		tip.newLine();

		tip.append(new TranslationTooltipInfo(InfoWS.info("progress")));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(Math.round(progress / (float) getCraftTime() * 100) + "%"));

		tip.newLine();
		if(inv.getStackInSlot(0).isEmpty())
			tip.append(new TranslationTooltipInfo(InfoWS.info("required.helmet.normal")));
		else if(inv.getStackInSlot(1).isEmpty())
			tip.append(new TranslationTooltipInfo(InfoWS.info("required.solar")));
		else
		{
			tip.append(new TranslationTooltipInfo(InfoWS.info("processing")));
			char[] chs = new char[(ticksExisted / 8) % 4];
			Arrays.fill(chs, '.');
			tip.append(new StringTooltipInfo(new String(chs)));
		}
	}
}